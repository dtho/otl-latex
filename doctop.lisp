(in-package :otlr)
;;;
;;; doctop.lisp
;;;
(defparameter *latex-document-class*
  ;; xelatex supports 'article', 'report'
  "\\documentclass{report}
  ")


;; ? xelatex can't handle ?
;; \\usepackage[pdftex]{graphicx}
;; \\usepackage{epstopdf}
;; \\usepackage{pdfpages}
;; \\usepackage{url}
;; \\usepackage[toc]{appendix}
;; \\usepackage{epigraph}
;; \\usepackage{booktabs}

;; ENUMERATE supports ordered alpha lists

(defparameter *latex-packages1*
  (concatenate 
   'string
   "
%
% tables:
% tabularx allows specification of table width and facilitates wrapping text content of cell
\\usepackage{tabularx}
% booktabs allows sane vertical spacing with horizontal rules
\\usepackage{booktabs}



%% - include this regardless so that LaTeX doesn't choke on Cref{...}
%% - the order of loading should be: amsmath → hyperref → cleveref
\\usepackage{amsmath}
\\usepackage{cleveref}

\\usepackage{enumerate}

% try and handle URLs intelligently
\\usepackage[obeyspaces,spaces]{url}

%% facilitate admonitions as nice, floating boxes
\\usepackage{float}
\\floatstyle{boxed}
\\newfloat{AdmonitionBox}{thp}{lop}
\\floatname{AdmonitionBox}{Glurp}


%% strikethrough support
\\usepackage{soul}

% booktabs allows better table styling w/sane vertical spacing with horizontal rules
\\usepackage{booktabs}

% support glossary generation (this needs to come after hyperref)
\\usepackage{glossaries}
\\makeglossaries

%
\\usepackage{verbatim}
"

   ;; support textblocks with multiple textlines at the same indent level
   "\\usepackage{parskip}"

   ;; support unicode characters in source file
   ;; - select default font families for entire document -> with relatively large coverage of anticipated unicode space
   "\\usepackage{fontspec}
\\setmainfont{DejaVu Serif}
\\setsansfont{DejaVu Sans}
\\setmonofont{DejaVu Sans Mono}
"
   ;; graphics (embedded)
"\\usepackage{graphicx}
\\usepackage{epstopdf}
\\usepackage{filecontents}
\\usepackage{ragged2e}
\\usepackage{setspace}
"

   ;; \textsuperscript and \textsubscript
   "\\usepackage{xltxtra}
"
   ))

(defparameter *latex-root-start-tag*
  "\\begin{document}
")

