(in-package :otlr)
;;;
;;; footnotes.lisp
;;;
(defun footnote-latex (object-sequence item-id item-specific stream)
  (declare (special %obj-seq% %obj-seq-pointer% %output-lang%)
	   (ignore item-id item-specific))
  ;; 2011 LaTeX \footnote doesn't support automatically inserting commas for a series of footnote marks
  (let ((next-obj (otlb:next-obj %obj-seq% %obj-seq-pointer%))
	(next-obj+1 (otlb:next-obj %obj-seq% (1+ %obj-seq-pointer%))))
    (write-string
     (concatenate 'string
		  "\\footnote{" 	    
		  (obj-seq-to-string object-sequence) 
		  "}")
     stream)
    ;; kludges
    
    ;; looks nicer to have numeric footnotes pushed up against preceding punctuation or word
    (if (eq #\Space (otlb:prev-obj %obj-seq% %obj-seq-pointer%))
	(setf (nth (1- %obj-seq-pointer%) %obj-seq%) nil))
    (let ((footnote-next-p (and (otlb:itemp next-obj) 
				(eq (otlb:get-item-key next-obj) :footnote)))
	  (space-then-footnote-p 
	   (and
	    (eq next-obj #\Space)
	    (otlb:itemp next-obj+1) 
	    (eq (otlb:get-item-key next-obj+1) :footnote))))
      (when (or footnote-next-p
		;; accomodate more readable source w/spaces between footnote indicators
		space-then-footnote-p) 
	  (write-string "\\textsuperscript{,}" stream)))))

(defun footnote-transform-latex ()
  (declare (special %obj-seq% %obj-seq-pointer%))
  ;; some markup languages aren't too bright/aesthetic when it comes to whitespace and footnote markers
  ;; modify previous item if it is space and is succeeded by a footnote item
  (let ((previtem (otlb:prev-obj %obj-seq% %obj-seq-pointer%)))
    (cond ((eq previtem #\Space)
	   (setf (nth (1- %obj-seq-pointer%) %obj-seq%)
		 nil))
	  ;; this (below) may actually be the way we want to go: managing numbering and marks ourselves rather than relying on LaTeX...

	  ;; ;; only for numeric lists... add comma to footnotes trailing other footnotes
	  ;; ((eq (otlb:get-item-key previtem) :footnote)
	  ;;  (setf (second (nth %obj-seq-pointer% %obj-seq%))
	  ;; 	 ;; push comma on to end
	  ;; 	 (append
	  ;; 	  (second (nth %obj-seq-pointer% %obj-seq%))
	  ;; 	  (list #\, #\Space))))
	  )))

