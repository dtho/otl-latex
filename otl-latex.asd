;;;
;;; otl-latex.asd 
;;;
(defsystem otl-latex
  :serial t
  :description "LaTeX rendering for OTL."
  :components ((:file "doctop")
	       (:file "render-latex") 
	       (:file "table")
	       (:file "footnotes")
	       (:file "vars-latex"))
  :depends-on (:otl
	       :log4cl
	       :puri
	       :cl-base64
	       :dfile
	       :dat-shell 
	       :dltx))
