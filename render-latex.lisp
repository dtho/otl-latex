(in-package :otlr)
;;;
;;; render-latex.lisp - LaTeX rendering
;;;
(defparameter *render-latex* 
  '((:ADMONITION . otlr::admonition-latex)
    (:BIBLIOGRAPHY . otlr::bibliography-x)
    (:BOLD . otlr::bold-latex)
    (:BRACKETED . otlr::bracketed-x)
    (:CHAR . dltx::lit-char)
    (:CHEM . otlr::chem-expr-latex)
    (:CITATIONS . otlr::citations-latex)
    ;(:CITE . otlr::citation-latex)
    (:CODE . dltx::code-latex)
    (:COMMENT . otlr::comment-x)
    (:DEFINITION . otlr::definition-latex)
    (:DOCBOTTOM . otlr::docbottom-x) 
    (:DOCTOP . otlr::doctop-latex)
    (:DOCUMENT . otlr::document-latex)	; not in dltx (yet?)
    (:EMPHASIS . dltx::emphasis-latex)
    (:EMPTYLINE . otlr::emptyline-x)
    (:ESC . otlr::escaped-x)
    ;;(:FOOTER . otlr::footer-latex)
    (:FOOTNOTE . otlr::footnote-latex)
    ;; footnotes are automatically rendered in LaTeX
    (:FOOTNOTES . nil ; dltx::footnotes-latex
     )
    (:FUNCTION . otlr::function-x)
    (:GLOSSARY . dltx::glossary-latex)
    (:GLOSSTERM . otlr::glossterm-latex)
    (:IMAGE . otlr::image-latex)	; not in DLTX (yet?)    
    (:INDEXTERM . otlr::indexterm-latex)
    (:ITALIC . otlr::italic-latex)	; ; not in DLTX (yet?)
    (:LINKPOINTER . otlr::linkpointer-latex) 
    ;; LINKTARGET items are dealt with prior to rendering (see RENDER-OBJECT-SEQUENCE)
    (:LINKTARGET . nil) 
    (:LIST . otlr::list-x)
    (:LIT . otlr::asis-x)
    (:LIT-CHAR . dltx::lit-char)
    (:MATH . otlr::math-expr-latex)
    (:MONOSPACE . otlr::monospace-latex)
    (:NEWLINE . dltx::newline-latex)
    (:NOOP . otlr::noop-x)
    (:PAGEBREAK . otlr::pagebreak-latex)
    (:POST . otlr::post-latex)		; not in DLTX (yet?)
    (:PREFORMATTED . otlr::preformatted-latex)
    (:SECTHEAD . otlr::secthead-latex)
    (:SMALLCAPS . dltx::smallcaps-latex)
    (:SMALLER . dltx::smaller-latex)
    (:STRIKETHROUGH . otlr::strikethrough-latex)
    (:STRONG . dltx::strong-latex)
    (:SUB . otlr::sub-latex)		; not in DLTX (yet?)
    (:SUP . otlr::sup-latex) 		; not in DLTX (yet?)
    (:TABLE . otlr::table-latex)
    (:TEXTBLOCK . otlr::textblock-x)
    (:TEXTLINE . otlr::textline-latex)
    (:TOC . dltx::toc-latex)
    (:UNDER . otlr::under-x)
    (:UNDERLINE . otlr::underline-latex) 
    (:URI . otlr::uri-latex)))

(add-to-*render-set* :latex *render-latex*)

(defparameter *render2-latex*
  (list
   '(:default
     ;; treat the top-level textblock as a paragraph
     
     ;; approaches:

     ;; 1. use an environment that establishes a paragraph (note: if this approach is used, ensure something like ragged2e is used so that hyphenation is supported)
;;      (" \\begin{flushleft} "
;; "  \\end{flushleft} ")


     ;; 2. use line breaks to define paragraphs -->
     ("

"
 
"

")

     ;; 3. use the \par command correctly:
     ;; FIXME: why does this cause problems?
     ;; ("
;; "
;; " \\par
;; ")




     ;; 3. bad idea: \par is a paragraph ending command; it shouldn't be used with an argument --> this doesn't work
     ;; ("\\par { " " }
;; ")
     

     ;; textblocks can be nested but nested \par element doesn't work well in LaTeX
     ;; - the quote environment behaves well when nested
     ("\\begin{quote}
" " 
\\end{quote}
 ")
     ("\\begin{quote}
" " 
\\end{quote}
 "))
   '(:empty " ")
   '(:foot otlr::docbottom-latex)

   ;; GLOSS-FILESPEC-GENERATOR

   '(:list
     (:ordered :numeric "\\item " " " "\\begin{enumerate}[1.]" "\\end{enumerate}" :o)
     (:ordered :alpha-upcase "\\item " " " "\\begin{enumerate}[A.]
" "
\\end{enumerate}")
     (:ordered :alpha-downcase "\\item " " " "\\begin{enumerate}[a.]
" "
\\end{enumerate}")
     ;; change default label to white/open square; escape [ ] since regex
     (:unordered :opensquare 
      "\\item " " " "{\\renewcommand{\\labelitemi}{□}\\begin{itemize}" "\\end{itemize}}")
     (:unordered :disc "\\item " " " "\\begin{itemize}" "\\end{itemize}"))
   
   '(:preequivalent "\\begin{verbatim}" "\\end{verbatim}") 

   '(:suffix "tex") 

   ;; preferred method of specifying table details (encourage development of a generic (HTML/LaTeX/DocBook/...) table-writing algorithm)
   (list :table
	 "&"			     ; CELL-SPEC:  & as cell separator
	 ""			     ; ROW-START
	 "\\\\
"				      ; ROW-END
	 ;; table body start
	 #'(lambda (rows-otl)
	     ;; N: number of columns
	     (let ((n (length (second (first rows-otl)))))
	       (with-output-to-string (s)
		 (write-string "\\begin{tabularx}{\\linewidth}{" s)
		 ;; COLS example: {l|l|l} for three-column table
		 ;; (l: left-centered; c: centered; r: right-aligned) 
		 
		 ;; is there a good rationale for a specific default for horizontal alignment of text in columns?
		 (dotimes (x (1- n))
		   (write-string "X" s))
		 (write-string "X}" s))))
	 "\\end{tabularx}"  ; table body end
	 "\\begin{table}\\small" 	; table start
	 "\\end{table}"				; table end
	 "&"		       ; header-cell-spec: & as cell separator
	 )
   '(:toc-generator 'otlr::toc-generator-latex)))

(add-to-*render2-set* :latex *render2-latex*)

(defun admonition-latex-ORIG (object-sequence item-id item-kvs stream)
  (declare (ignore item-id))
  (let* ((type (otlb::item-kvs-val :type item-kvs))
	 (type-string (otlb::char-seq-to-string type)))
    (assert (member type-string '("note" "hint" "tip" "warning") :test #'string=))
    ;; FIXME: use 'marginnote' package? use 'fixme' package? 
    (dltx:l "marginpar"
	       (concatenate 'string
		       ;; FIXME: clumsy styling -- margin notes look better in different font family and/or size
		       "
\\fontspec{FreeSerif}
\\begin{spacing}{0.9}
\\begin{FlushLeft}
"
		       type-string
		       ": "
		       (obj-seq-to-string object-sequence)
		       "\\end{FlushLeft}
\\end{spacing}"
		       )
	  :s stream)))

(defun bold-latex (object-sequence item-id item-specific stream)
  (declare (ignore item-id item-specific))
  (dltx:l "textbf" (obj-seq-to-string object-sequence) :s stream))

(defun chem-expr-latex (object-sequence item-id item-kvs stream)
  (declare (ignore item-id item-kvs))
  ;; assume LaTeX using chemfig package
  (chem-expr-latex-chemfig object-sequence stream))

(defun chem-expr-latex-chemfig (object-sequence stream)
  (write-string "\\chemfig{" stream)
  (otlb:charlist-to-stream object-sequence stream)
  (write-string "}
" stream))

(defun citation-latex (object-sequence item-id item-kvs stream &key (refdbp nil))
  ;; when :CITE item encountered (RENDER-CITATION), 
  ;;   1. unique ID is stored in *BIB-IDS*
  ;;   2. citation/note generated in appropriate format
  (declare (ignore object-sequence item-id refdbp))
  (dltx:l "cite" (obj-seq-to-string (otlb::item-kvs-val :cid item-kvs)) :s stream))

(defun citations-latex (object-sequence item-id item-kvs stream &key (refdbp nil))
  ;; when :CITATIONS item encountered (RENDER-CITATION),
  ;;   1. unique ID is stored in *BIB-IDS*
  ;;   2. citation/note generated in appropriate format
  (declare (ignore object-sequence item-id refdbp))
  (let ((citation-items (otlb::item-kvs-val :citations item-kvs)))
    ;; CITATION-ITEMS looks like (((:CITE NIL :CID (#\A #\l #\l #\e #\y #\: #\9 #\6))) ...
    (dltx:l "cite"
	    (with-output-to-string (s)
	      (dolist (cite-item citation-items)
		(obj-seq-to-stream (otlb::item-kvs-val :cid (otlb::get-item-item-kvs cite-item))
				   s)
		(write-char #\, s)))
	    :s stream)
    ;; since the parser sucks up trailing whitespace, we deal with it here
    (obj-seq-to-stream (otlb::item-kvs-val :trailing-whitespace item-kvs)
		       stream)))

(defun definition-latex (object-sequence item-id item-kvs stream)
  (declare (ignore object-sequence item-id))
  (write-string "\\begin{description}
" stream)
  (dltx:l "item"
	  nil
	  :optional-arg (obj-seq-to-string (otlb::item-kvs-val :term item-kvs))
	  :s stream)
  ;; LaTeX won't automatically insert this space
  (write-char #\Space stream)
  (render-obj-seq (otlb::item-kvs-val :definition item-kvs) stream)
  (write-string "\\end{description}" stream))
  
(defun docbottom-latex ()
  (declare (special %output-subtype%))
  (cond ((eq %output-subtype% :section)
	 "")
	((eq %output-subtype% :document)
	 "\\end{document}")
	(t (error "You must specify :OUTPUT-SUBTYPE with LaTeX output."))))

;; partree :OUT specifies output type
(defun doctop-latex (object-sequence item-id item-kvs stream)
  (declare (ignore object-sequence item-id item-kvs stream) 
	   (special %glossp% %output-subtype%)
	   (stream stream))
 ;;  (cond ((eq %output-subtype% :section)
;; 	 ;;""
;; 	 )
;; 	((eq %output-subtype% :document)
;; 	 ;; preamble 
;; 	 (write-string *latex-document-class* stream)
;; 	 ;; if GLOSSP then preamble should have \usepackage{glossaries} and \makeglosaries
;; 	 (write-string *latex-packages1* stream)
;; 	 (if %glossp%
;; 	     (write-string 
;; 	      (concatenate 'string
;; 			   "\\usepackage{glossaries}
;; \\makeglossaries 
;; \\loadglsentries{" (namestring *gloss-filespec*) ;; newglossentries 
;; "}
;; ") stream)
;; 	     ;;""
;; 	     )
;; 	 ;; document
;; 	 (write-string *latex-root-start-tag* stream))
;; 	(t (error "You must specify :OUTPUT-SUBTYPE with LaTeX output.")))
  )

;; partree :OUT specifies output type
(defun document-latex (object-sequence item-id item-kvs stream)
  (declare (ignore item-id) 
	   (special %glossp% %output-subtype% %obj-seq%)
	   (stream stream))
  (multiple-value-bind (author date-last-modified date-created paper-size title)
      (otlb::document-item-kvs-as-values 
       item-kvs
       ;; fallback: grab title as the text of second object in the document object sequence
       :title-default (otlb::top-level-chars-to-string (otlb:get-item-obj-seq (second %obj-seq%))))
    (declare (ignore date-last-modified date-created))
    (cond ((eq %output-subtype% :section)
	   ;;""
	   )
	  ((eq %output-subtype% :document)
	   ;; FIXME: specify PAPER-SIZE
	   ;; - usually specified in \documentclass[ ] declaration -- e.g,. \documentclass[11pt,twoside,a4paper]{article}


	   ;; FIXME: specify AUTHOR  TITLE
	   ;; - these typically go in top matter as \author{j doe} and \title{Big title}


	   ;; preamble 
	   ;;(write-string *latex-document-class* stream)
	   (dltx:documentclass 
	    "report"
	    :opts-string (cond ((not paper-size)
				"")
			       ((eq (clps::paper-size-nickname paper-size) :a4)
				"a4paper")
			       ((eq (clps::paper-size-nickname paper-size) :letter)
				"letter")
			       (t (error "Unrecognized paper size")))
	    :stream stream)

	   (if author (dltx:l "author" author :s stream))
	   (if title (dltx:l "title" title :s stream))


	   ;; if GLOSSP then preamble should have \usepackage{glossaries} and \makeglosaries
	   (write-string *latex-packages1* stream)
	   (if %glossp%
	       (write-string 
		(concatenate 'string
			     "\\usepackage{glossaries}
\\makeglossaries 
\\loadglsentries{" (namestring *gloss-filespec*) ;; newglossentries 
			     "}
") stream)
	       ;;""
	       )
	   ;; document
	   (write-string *latex-root-start-tag* stream))
	  (t (error "You must specify :OUTPUT-SUBTYPE with LaTeX output.")))
    (render-obj-seq object-sequence stream)
    ))

(defun emphasis-latex (object-sequence item-id item-specific stream)
  (declare (ignore item-id item-specific))
  (dltx:l "emph" (obj-seq-to-string object-sequence) :s stream))

(defun glossary-latex (object-sequence item-id item-kvs stream)
  (declare (ignore object-sequence item-id item-kvs))
  (dltx:l "printglossary" nil :s stream))

(defun glossterm-latex (object-sequence item-id item-kvs stream)
  (declare (ignore object-sequence item-id))
  ;; TERM: an object sequence
  ;; DEF: an object sequence
  (let ((term (otlb::item-kvs-val :term item-kvs))
	(singular? (otlb::item-kvs-val :singular item-kvs))
	(def (otlb::item-kvs-val :def item-kvs)))
    (let ((singular (or singular? term)))
    (push (list (or singular term) def) *gloss-stack*)  
    ;; it may be desirable to render \gls even if glossary itself isn't rendered in the context of otl (include terms even if --gloss flag isn't passed)
    ;; it is not desirable to render \gls if --glossfile flag wasn't passed to otl (see RENDER-DOCUMENT and RENDER-RESET)
    (if *gloss-filespec*
	;; FIXME: first time? or every time term is encountered?

	;; use keys which are alphanumeric but support complex formatting
	;; \glsdisp[⟨options⟩]{⟨label⟩}{⟨link text⟩}
	;; (format stream 
	;; 	"\\glsdisp{~A}{~A}~%" 
	;; 	(fix-glossterm-term term)
	;; 	(obj-seq-to-string term))

	;; \\gls
	(dltx:l "gls"
	      ;; what to do if we have gloss:[foo] but foo is also an index term? 
	      ;; -> current approach (below) isn't quite ideal since we lose the indexterm information if first member of TERM looks like ((:INDEXTERM NIL) (#\r #\e #\g #\r #\e #\s #\s #\i #\o #\n))
	      (fix-glossterm-term term)
	      :s stream)
	(render-obj-seq term stream)))))

;; ITEM-ID: see doc/internal-representation/item-spec.txt
(defun image-latex (object-sequence item-id item-kvs stream)
  "PATH is a string or a path object corresponding to an image."
  (declare (ignore object-sequence)
	   (special %output-lang%)
	   (stream stream))
  (log:debug item-id item-kvs stream)
  ;; FIXME: use parfile to specify image markup
  (let ((uri (otlb::item-kvs-val :uri item-kvs))
	(title (otlb::item-kvs-val :title item-kvs))
	;; see doc/images-figures.txt
	(type (or (otlb::item-kvs-val :type item-kvs) :image))
	(id-clean (otlb::acceptable-id item-id)))
    ;; here, we assume graphicx is being used (with epstopdf w/on-the-fly conversion)
    ;; FIXME: warn if absolute path isn't used -- relative paths are finicky in LaTeX
    (let* ((caption (otlb::item-kvs-val :caption item-kvs))
	   (puri-uri (puri:parse-uri uri))
	   (uri-scheme (puri:uri-scheme puri-uri)))
      (log:debug caption title)
      (cond ((eq uri-scheme :data)
	     (image-latex-data-uri :caption caption :id id-clean :stream stream :title title :type type :uri uri))
	    ((eq uri-scheme :file) 
	     (let ((path (puri:uri-path puri-uri)))
	       (let ((path-object (if (pathnamep path) path (pathname path)))
		     (path-string (if (pathnamep path) (namestring path) path)))
		 ;; FIXME: assume xelatex or pdflatex in use
		 (unless (member (pathname-type path-object) *image-formats-latex* :test #'equalp)
		   ;; FIXME: better to offer opportunity to try again (give user change to generate image in an acceptable format)
		   ;;(acceptable-image-p path-object %output-lang%)
		   (cerror "Continue"
			   (format nil "Image ~A is not in an acceptable format (see OTLR::*IMAGE-FORMATS-LATEX*) for LaTeX output." path-object)))
		 (let ((path-dir (pathname-directory path-object))) 
		   (cond ((eq type :image)
			  (write-string
			   (cond ((eq (first path-dir) :absolute)
				  (dltx:includegraphics path-string))
				 (t
				  "*** use absolute paths with latex output ***"))
			   stream))
			 ((eq type :figure) 
			  (cond ((eq (first path-dir) :absolute) 
				 (dltx:figure 
				  id-clean path-string
				  ;; the LaTeX figure caption is distinct from the otl figure caption
				  ;; - LaTeX defines the caption as all of the text succeeding 'Figure' and the number
				  ;; - otl (current implementation documentation) indicates that the text succeeding 'Figure' and the number should first be the title and then the caption
				  :caption (cond (caption
						  (concatenate 'string
							       ;; . character may be present or absent at end of title
							       (string-right-trim "." (obj-seq-to-string title))
							       ". "
							       (obj-seq-to-string caption)))
						 (t
						  ;; For a figure, if a caption isn't specified use the title.
						  (obj-seq-to-string title)))
				  :stream stream
				  :title (obj-seq-to-string title)))
				(t (write-string "*** use absolute paths with LaTeX output ***" stream))))
			 (t (error (format nil "Unknown type ~S" type)))))))) 
	    ((not uri-scheme)
	     (error "IMAGE/FIGURE URI must have an explicit scheme (e.g., file, http, etc.)"))
	    (t
	     (error (format nil "URI scheme ~A not supported" uri-scheme)))))))

;; STREAM must be an output stream
(defun image-latex-data-uri (&key caption id stream title type uri)
  (declare (stream stream))
  ;; FIXME: for a URI like "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQ...", PURI-UNICODE seems to bungle parsing and gives, for URI-PATH, "imagepng;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg=="
  ;; restrict to png images with base 64 encoding
  (unless (string= (subseq uri 0 21) "data:image/png;base64")
    (error "Only base64-encoded PNG handled for now..."))
  (let ((base64-string (subseq uri 22))
	(safe-tmp-path 
	 (dfile:make-unique-path (dfile:tempdir)
				 :createp nil
				 :suffix "png")))
    ;; 1. write png to tmp file 
    (base64-string-to-file base64-string safe-tmp-path)
    ;; 2. convert to EPS
    (let ((safe-tmp-path-eps
	   (merge-pathnames (make-pathname :type "eps")
			    safe-tmp-path)))
      ;; FIXME: relying on this shell command seems dicey unless we check for its existence up-front and warn if not present; the alternative, CL-MAGICK (6 y old), seems broken and porting Franz's is likely to be a hassle...
      (format t "SAFE-TMP-PATH: ~S~%SAFE-TMP-PATH-EPS: ~S~%" safe-tmp-path safe-tmp-path-eps)
      (multiple-value-bind (stdout-string status)
	  (dat-shell:shell-command "convert" 
				   :args 
				   (list (namestring safe-tmp-path)
					 (namestring safe-tmp-path-eps)))
	(declare (ignore stdout-string))
	(unless (= 0 status) (error "Error in shell-level conversion")))
      ;; 3. embed EPS: \begin{filecontents*}{redsmudge-from-tex.eps} ...
      (let ((safe-tmp-path-name 
	     (concatenate 'string (pathname-name safe-tmp-path) ".eps")))
	(dltx:env "filecontents*"
		 (otlb::file-to-string safe-tmp-path-eps)
		 :opts-string safe-tmp-path-name
		 :stream stream		; no directory
		 )
	;; 4. \includegraphics
	(cond ((eq type :image)
	       (write-string (dltx:includegraphics safe-tmp-path-name) stream))
	      ((eq type :figure)
	       (dltx:figure 
		id 
		safe-tmp-path-name
		:caption caption
		:stream stream
		:title title))
	      (t (error "Unknown type")))))))


(defun indexterm-latex (object-sequence item-id item-kvs stream)
  (declare (ignore item-id item-kvs))
  (let ((actual-word (otlb:charlist-to-string object-sequence))
	(indexterm (otlb:charlist-to-string object-sequence)))
    (write-string actual-word stream)
    (dltx:l "index" indexterm :s stream)))

(defun italic-latex (object-sequence item-id item-specific stream)
  (declare (ignore item-id item-specific))
  (dltx:l "textit" (obj-seq-to-string object-sequence) :s stream))

(defun linkpointer-latex (object-sequence item-id item-kvs stream)
  (declare (ignore item-id object-sequence))
  (let ((to-id (otlb::item-kvs-val :to-id item-kvs)))
    (if to-id
	(dltx:l "Cref" to-id :s stream) ; cleveref package "\\ref{"
	)))

(defun math-expr-latex (object-sequence item-id item-kvs stream)
  (declare (ignore item-id item-kvs))
  ;; if first char is '$', assume LaTeX math
  ;; if first char is '<', assume MathML
  (cond ((eq #\$ (first object-sequence))
	 (otlb:charlist-to-stream object-sequence stream))
	((eq #\< (first object-sequence))
	 ;; (math-expr-latex-mathml ...)
	 ;; - use [ ? ]
	 (error "MathML is unsupported format with LaTeX output."))
	(t
	 (error "Unsupported math expression format in source document."))))

(defun monospace-latex (object-sequence item-id item-specific stream)
  (declare (ignore item-id item-specific))
  (dltx:l "texttt" (obj-seq-to-string object-sequence) :s stream))

;; See comments for TEXTLINE-LATEX
(defun newline-latex (object-sequence item-id item-kvs stream)
  (declare (ignore object-sequence item-id item-kvs))
  (write-string "
\\
" stream))

(defun pagebreak-latex (object-sequence item-id item-kvs stream)
  (declare (ignore object-sequence item-id item-kvs))
  (dltx:l "pagebreak" nil :optional-arg "99" :s stream))

(defun post-latex ()
  "Actions to perform after rendering."
  ;;
  ;; glossary support
  ;;
  ;; generate file with \newglossentry clauses
  (if (and *gloss-stack* *gloss-filespec*)
      ;; write to a specified file, appending or generating file from scratch
      (with-open-file (stream *gloss-filespec* :direction :output
			      :if-exists (if t ; *gloss-append-to-file-p*
					     :append
					     :supersede)
			      :if-does-not-exist :create)
	(map nil #'(lambda (gloss)
		     (let ((term (fix-glossterm-term (first gloss)))) 
		       (format stream 
			       "\\newglossaryentry{~A}{
 name={~A},
 description={~A},
 plural=~A,
 sort=~A}
"
			       term	; label (the key value)
			      
			       ;; name: how term is rendered in the glossary
			       ;; - desirable to accomodate complex stuff (e.g., pKa) 
			       ;; - but need to filter out \index{} markup if present 
			       (obj-seq-to-string (first gloss) 
						  ;; custom renderer for :indexterm
						  (list :indexterm
							#'(lambda (obj-seq stream)
							    (otlb::top-level-chars-to-stream obj-seq stream))))
			       
			       ;; term	; loses complex markup
			       
			       (obj-seq-to-string (second gloss)) ; description: how term is rendered in text
			       "FIXME"				 ; plural
			       term ; sort
			       )))
		     *gloss-stack*))))

;; attempt to work around "Paragraph ended before \xverbatim@ was complete" by putting verbatim content in another file and reading with \verbatiminput (provided by the verbatim package)
(defun preformatted-latex (object-sequence item-id item-kvs stream &key (use-file-p t))
  "Preformatted text. OBJECT-SEQUENCE must be composed exclusively of characters."
  (declare (ignore item-id item-kvs)
	   (special %out-dir%))
  (let ((preequiv-start-stop (get-*render2*-value :preequivalent))
	(preformatted-content-string (otlb::charlist-to-string object-sequence)))
    (if use-file-p
	;; %OUT-DIR% has the form '(:ABSOLUTE "path" "to" "somewhere")
	(let ((tmppath (dfile:make-unique-path (make-pathname :directory %out-dir%) :createp nil)))
	  (dfile:string-to-file preformatted-content-string tmppath)
	  (write-string (format nil "\\verbatiminput{~A}" (namestring tmppath)) stream))
	(write-string
	 (concatenate 'string
		      (first preequiv-start-stop)
		      preformatted-content-string
		      (second preequiv-start-stop))
	 stream))))

(defun secthead-latex (object-sequence item-id item-kvs stream)
  (declare (ignore item-id)
	   (special otlr::%output-subtype%))
  (let ((sign (otlb::item-kvs-val :sign item-kvs)))
    (let ((hstring 
	   (cond ((eq otlr::%output-subtype% :document)
		  (case sign
		    (1 "section")
		    (2 "subsection")
		    (3 "subsubsection")
		    (4 "paragraph")))
		 ((eq otlr::%output-subtype% :section)
		  (case sign
		    (1 (error "This shouldn't be present when using %output-subtype% :section"))
		    (2 "section")
		    (3 "subsection")
		    (4 "subsubsection")
		    (5 "paragraph")))
		 (t (error "Unsupported %OUTPUT-SUBTYPE% value")))))
      (dltx:l hstring
	    (obj-seq-to-string object-sequence)
	    :s stream))))

(defun smallcaps-latex (object-sequence item-id item-specific stream)
  (declare (ignore item-id item-specific))
  (dltx:l "textsc" (obj-seq-to-string object-sequence) :s stream))

(defun smaller-latex (object-sequence item-id item-specific stream)
  (declare (ignore item-id item-specific))
  (dltx:l "small" (obj-seq-to-string object-sequence) :s stream))

(defun strikethrough-latex (object-sequence item-id item-specific stream)
  (declare (ignore item-id item-specific))
  ;; this requires the soul package
  (dltx:l "st" (obj-seq-to-string object-sequence) :s stream))

;; \textsubscript requires xltxtra (see *LATEX-PREAMBLE-MISC*)
(defun sub-latex (object-sequence item-id item-specific stream)
  (declare (ignore item-id item-specific))
  (dltx:l "textsubscript"
	  (obj-seq-to-string object-sequence)
	  :s stream
	  :suppress-newline-p t))

;; \textsuperscript requires xltxtra (see *LATEX-PREAMBLE-MISC*)
(defun sup-latex (object-sequence item-id item-specific stream)
  (declare (ignore item-id item-specific))
  (dltx:l "textsuperscript"
	  (obj-seq-to-string object-sequence)
	  :s stream
	  :suppress-newline-p t))



;; FIXME: handle textblock composed of multiple textlines of the same tablevel
(defun textblock-latex (object-sequence item-id item-kvs stream)
  (declare (ignore item-id))
  (let ((tablevel? (otlb::item-kvs-val :tablevel item-kvs)))
    (let ((textblock-pairs (textblock-pairs))
	  (tablevel (if (integerp tablevel?) 
			tablevel? 
			0)))
      (let ((tb-pair (if (>= (length textblock-pairs)
			     (1+ tablevel))
			 (nth tablevel textblock-pairs))))
	(if tb-pair 
	    (write-string (first tb-pair) stream))
	(render-obj-seq object-sequence stream)	
	(if tb-pair 
	    (write-string (second tb-pair) stream))))))

;; See also NEWLINE-LATEX
(defun textline-latex (object-sequence item-id item-kvs stream)
  (textline-x object-sequence item-id item-kvs stream
	      ;; // is problematic as a means of generating line breaks. A line like "blabla // [] foo" can bring LaTeX to its knees.
	      ;;:line-break-string " \\\\ "
	      :line-break-string " \\newline "
	      ;; If \newline proves problematic, consider options
	      ;; (e.g., obeylines) described in
	      ;; latex/hyphenation--line-breaks/line-breaks.org and
	      ;; document any shortcomings as we go.
	      ))

(defun textline-latex-OLD (object-sequence item-id item-kvs stream)
  (declare (ignore item-id)
	   (special %containing-obj-seq% %containing-obj-seq-pointer%))
  (let ((tablevel (otlb::item-kvs-val :tablevel item-kvs)))
    ;; an indented textline succeeding another textline is part of a textblock
    ;; - at this point, we treat a textblock as a \par { ... }
    ;; - the quote environment 'works' inside \par for indenting a piece of text
    
    ;; specify with PARTREE :DEFAULT ?
;;     (if tablevel
;; 	(write-string
;; 	 (cond ((eq tablevel 0) 
;; 		;;"\\par { "
;; 		"{"
;; 		)
;; 	       ((eq tablevel 1) 
;; 		;;"\\par{ \\narrower "
;; 		;;"\\begin{quote} {"
;; "{"
;; 		)
;; 	       ((eq tablevel 2) 
;; 		;;"\\par{ \\narrower \\narrower "
;; 		;;"\\begin{quote} {" 
;; "{"
;; 		)
;; 	       ((eq tablevel 3)
;; 		;;"\\par{ \\narrower \\narrower \\narrower "
;; 		;;"\\begin{quote} {"
;; "{"
;; 		)
;; 	       (t "{"))
;; 	 stream))
    (render-obj-seq object-sequence stream)

    ;; handle a series of textlines of the same indent within a single textblock
    ;; ISSUE: this approach to handling multiple textlines of the same tablevel within a textlock only is aesthetically pleasing if paragraphs don't have the first line indented 
    ;; -->> use the parskip package
    ;; -->> must check if line break is warranted -- can't simply put this indiscriminately everywhere (textlines show up in all sorts of places)
    (if (let ((prev-item (if (> %containing-obj-seq-pointer% 0)
			     (nth (1- %containing-obj-seq-pointer%)
				  %containing-obj-seq%)))
	      (next-item (if (< %containing-obj-seq-pointer% (1- (length %containing-obj-seq%))) 
			     (nth (1+ %containing-obj-seq-pointer%)
				  %containing-obj-seq%))))
	  (or (and prev-item
		   (otlb::itemp prev-item :textline)
		   (otlb::item-tablevel-p prev-item tablevel))
	      (and next-item
		   (otlb::itemp next-item :textline)
		   (otlb::item-tablevel-p next-item tablevel))))
	(write-string " \\\\ " stream))

    ;;(write-char #\} stream)
    ))

(defun toc-latex (object-sequence item-id item-kvs stream)
  (declare (ignore object-sequence item-id item-kvs))
  ;; \tableofcontents with tocloft package (used by memoir, ...)
  (dltx:l "tableofcontents" nil :s stream))

(defun underline-latex (object-sequence item-id item-specific stream)
  (declare (ignore item-id item-specific))
  (dltx:l "underline" (obj-seq-to-string object-sequence) :s stream))

(defun uri-latex (object-sequence item-id item-kvs stream)
  (declare (ignore item-id))
  (let ((hyperrefp t)			; T if using hyperref package
	(scheme (otlb::item-kvs-val :scheme item-kvs))
	(item-specifier-stuff (otlb::item-kvs-val :item-specifier-stuff item-kvs)))
    (let ((post-colon-string (otlb:charlist-to-string item-specifier-stuff))) 
      ;; with LaTeX, %URI-VISIBLE-P% is only of interest if we use a package which provides actual hyperlinks (e.g., in pdf documents)
      ;;(if %uri-visible-p% ...)
      ;; the URL package is smart enough to handle special characters inside of URLs...
      ;; (with-output-to-string (s)
      ;;   (lit-chars-latex post-colon-string s))
      
      ;; Documentation for the url package indicates:
      ;; If the argument contains any “%”, “#” or “^^”, or ends with “\”, it can’t be used in the argument to another command. The argument must not contain unbalanced braces.
      ;; Thus, the combination of \usepackage{hyperref} and \usepackage[obeyspaces,spaces]{url} causes LaTeX to choke on a URL containing the fragment delimiter (#).
      (when hyperrefp
	;; Locate first “%”, “#” or “^^” character. Remove everything after it.
	(setf post-colon-string
	      (first (cl-ppcre:split "%|#|\\^" post-colon-string)))
	;; Check if string terminates with \. If so, remove it.
	(setf post-colon-string
	      (string-right-trim "\\" post-colon-string)))
      (let ((uri-string (concatenate 'string scheme ":" post-colon-string)))
	;; use url
	;; - if OBJECT-SEQUENCE is true, then use it as the true label
	(cond (object-sequence
	       ;; the label (OBJECT-SEQUENCE) may have all sorts of otl formatting (which should be honored...)
	       (render-obj-seq object-sequence stream)
	       (write-string " (" stream)
	       (dltx:l "url" uri-string :s stream)
	       (write-char #\) stream))
	      ;; if OBJECT-SEQUENCE is absent, just render the URL (note above comment with respect to %URI-VISIBLE-P%)
	      (t
	       (dltx:l "url" uri-string :s stream)))))))

;;;
;;; base64 <-> file utilities
;;;

;; example: copy a png
;;     (otlr::base64-string-to-file (otlr::file-to-base64-string "/home/thomp/computing/lisp/src/otl/tests-markup/otl/images/dot.png") "/home/thomp/computing/lisp/src/otl/tests-markup/otl/images/dot-copy.png" :if-exists :overwrite)
(defun file-to-base64-string (infile)
  (let ((usb8-array (dfile::file-to-vector infile)))
      (cl-base64:usb8-array-to-base64-string usb8-array)))

(defun base64-string-to-file (base64-string outfile &key if-exists)
  (dfile::vector-to-file 
    (cl-base64:base64-string-to-usb8-array base64-string)
    outfile
    :if-exists if-exists))

;; testing/development
(defun admonition-latex (object-sequence item-id item-kvs stream)
  (declare (ignore item-id))
  (let* ((type (otlb::item-kvs-val :type item-kvs))
	 (type-string (otlb::char-seq-to-string type)))
    (assert (member type-string '("note" "hint" "tip" "warning") :test #'string=))
    ;; rely on custom command via the float package (see document preamble)
    (write-string      "
\\begin{AdmonitionBox}
" stream) 
    (obj-seq-to-stream object-sequence stream nil)
    (write-string      "
\\end{AdmonitionBox}
" stream)))
