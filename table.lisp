(in-package :otlr)
;;;
;;; table.lisp
;;;

;; FIXME: generate and then merge a truly genericized version with OTL-HTML and OTLR

;; TITLE: an object sequence
;; CAPTION: an object sequence
;; TCOLLBLS: a :TCOLLBLS item -- example: ((:TCOLLBLS NIL) (((:TROW NIL) ((#1=(:TCELL NIL) (#\c #\o #\l #\A #\  #\Tab)) (#1# (#\c #\o #\l #\B #\Tab)) ((:TCELL NIL) (#\c #\o #\l #\C))))))
(defun render-table-latex (rows-otl &key id stream title tcollbls caption)
  ;; ttitle tcollbls tcaption are the corresponding otl ITEMs
  (declare (ignore id)
	   (optimize (debug 3)
		     (safety 3)))
  (log:debug rows-otl tcollbls)
  (destructuring-bind (cell-spec row-start row-end-string table-body-start table-body-end-string table-start table-end-string head-cell-spec)
      (get-*render2*-value :table)
    ;; use title as default for ID 
    (let ((id-clean (otlb::acceptable-id 
		     (otlb::obj-seq->item-id-string title)))
	  ;; defaults if implementation doesn't provide them
	  (table-start (or table-start "\\begin{table}"))
	  (table-end (or table-end-string "\\end{table}"))
	  (table-body-start 
	   (or table-body-start
	    #'(lambda (rows-otl)
		;; N: number of columns
		(let ((n (length (second (first rows-otl)))))
		  (with-output-to-string (s)

		    ;;   tabularx: allows one to limit table width to a sane value 
		    ;;      \begin{tabularx}{<table width>}{<column specs>}
		    (write-string "\\begin{tabularx}{\\linewidth}{" s)
		    ;; COLS example: {l|l|l} for three-column table
		    ;; (l: left-centered; c: centered; r: right-aligned) 
		    
		    ;; is there a good rationale for a specific default for horizontal alignment of text in columns?

		    ;; tabularx X encourages an attempt at sane text wrapping within each cell
		    (dotimes (x (1- n))
		      (write-string "X" s))
		    (write-string "X}" s))))))
	  (table-body-end-string (or table-body-end-string "\\end{tabularx}"))
	  )
      
      ;; LaTeX-specific table markup
      ;; - assume \usepackage{tabular}

      (let ((table-start-string
	     (if (stringp table-start)
		 table-start
		 (funcall table-start rows-otl))))
	;; table start
	(write-string table-start-string stream)


	(write-string "\\begin{center}" stream)

	;; beginning of TABLE BODY
	(write-string (if (stringp table-body-start)
			  table-body-start
			  (funcall table-body-start rows-otl))  
		      stream)
	
	;; render table column labels
	(when tcollbls 
	  #+DAT-DEV (dd:dpr 3 "RENDER-TABLE-LATEX.50")
	  
	  (let ((tcollbls-rows (otlb:get-item-obj-seq tcollbls)))

	    (map nil
		 #'(lambda (tcollbls-row)
		     #+DAT-DEV (dd:dpr 3 "RENDER-TABLE-LATEX.52: tcollbls-row" tcollbls-row)

		     (write-string row-start stream)
		     (let ((tcollbl-cells (otlb:get-item-obj-seq tcollbls-row))) 
		       (cond ((consp head-cell-spec)
			      (let ((head-cell-start-string (first head-cell-spec))
				    (head-cell-end-string (second head-cell-spec)))
				(dolist (tcollbl-cell tcollbl-cells)
				  (write-string head-cell-start-string stream)
				  (render-obj-seq (otlb:get-item-obj-seq tcollbl-cell) stream)
				  (write-string head-cell-end-string stream))))
			     ((stringp head-cell-spec)
			      (let ((n-tcollbl-cells (length tcollbl-cells)))
				(dotimes (i (1- n-tcollbl-cells))
				  (render-obj-seq (otlb:get-item-obj-seq 
						   (elt tcollbl-cells i)) stream)
				  (write-string head-cell-spec stream)) 
				(render-obj-seq (otlb:get-item-obj-seq 
						 (elt tcollbl-cells (1- n-tcollbl-cells)))
						stream)))))
		     (write-string row-end-string stream))
		 tcollbls-rows))
	  ;; \hline is accompanied by vertical spacing issues -> use \midrule (provided by booktabs package) 
	  (dltx:l "midrule" nil :s stream :suppress-newline-p nil)
	 ;;  (write-string "\\hline
;; " stream)
	  )
	
	;; render table rows (LaTeX)
	;; this should be done in order if writing to stream -> use DO (not MAP)
	(dolist (row-otl rows-otl) 
	  (write-string row-start stream)
	  (let ((tcells (otlb:get-item-obj-seq row-otl)))
	    (cond ((consp cell-spec)
		   (let ((cell-start-string (first cell-spec))
			 (cell-end-string (second cell-spec)))
		     ;; process cells in order
		     (dolist (tcell tcells)
		       (write-string cell-start-string stream)
		       (render-obj-seq (otlb:get-item-obj-seq tcell) stream)
		       (write-string cell-end-string stream))))
		  ((stringp cell-spec)
		   (let ((n-tcells (length tcells)))
		     (dotimes (i (1- n-tcells))
		       (render-obj-seq (otlb:get-item-obj-seq (elt tcells i)) stream)
		       (write-string cell-spec stream))
		     
		     (render-obj-seq (otlb:get-item-obj-seq (elt tcells (1- n-tcells))) stream)))))
	  (write-string row-end-string stream))
	;; end TABLE BODY
	(write-string table-body-end-string stream)
	(write-string "\\end{center}" stream)


	;; LaTeX: title and caption withing \caption{...}
	(when (or title caption)
	  (write-string "\\caption{" stream)
	  (when title
	    (otlr::obj-seq-to-stream title stream nil))
	  (when caption
	    (otlr::obj-seq-to-stream caption stream nil))
	  (write-char #\} stream))

	(if id-clean 
	    (write-string (concatenate 'string "\\label{" id-clean "}
") stream))
	(write-string table-end stream) ; \end{table}
	))))

(defun table-latex (object-sequence item-id item-kvs stream) 
  ;; grab title, caption, column labels, row objects
  (let ((title (otlb::item-kvs-val :title item-kvs))
	(caption (otlb::item-kvs-val :caption item-kvs))
	(col-lbls (otlb::item-kvs-val :header-rows item-kvs))
	(rows-otl (otlb:elts-with-item-key object-sequence :trow))) 
    (render-table-latex rows-otl 
			:title title 
			;; COL-LBLS should be a :TCOLLBLS item
			:tcollbls col-lbls 
			:caption caption :id item-id :stream stream)))

