(in-package :otlr)

;; called post-rendering to insert TOC
(defun toc-generator-docbook ()
  ;; the DocBook stylesheets support automatic toc generation - no need to do anything here...
  "")