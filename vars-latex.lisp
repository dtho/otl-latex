(in-package :otl)
;;
;; vars-latex.lisp
;;

;; FIXME: used?
(defparameter *TRANSFORM-latex*
  '((:FOOTNOTE . otlr::footnote-transform-latex)))

(if (assoc :latex otlb::*transform-trees*)
    (setf (cdr (assoc :latex otlb::*transform-trees*)) (list *transform-latex*))
    (push (list :latex *transform-latex*) otlb::*transform-trees*))

;;;==========================================
;;;
;;; define output markup-generating functions
;;;
(in-package :otlr)

;; LaTeX image handling isn't uniform
;; - xelatex (PDF):
;; - pdflatex (PDF) workflow: native support for png, jpeg, PDF, ......
;; - latex (dvi) workflow: convert to eps in advance for best results
(defparameter *image-formats-latex* 
  '(
    ;; xelatex doesn't handle svg, tif, ...
    "jpg" "pdf" "png" 		
    ;; with TeX-ish stuff, suffix/type is often not specified and latex/pdftex/xetex/... is relied upon to look for foo.pdf or foo.ps or whatever...
    "" nil))

;; FIXME: used?
(let ((out :latex))
  (if (assoc :latex otlr::*RENDER-IMAGE*)
      (setf (cdr (assoc out otlr::*render-image*)) (list *image-formats-latex*))
      (push (list out *image-formats-latex*) otlr::*render-image*)))
